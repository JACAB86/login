package com.jaom.ws.soap;

import com.jaom.ws.trainings.GetUserRequest;
import com.jaom.ws.trainings.GetUserResponse;
import com.jaom.ws.trainings.User;
import com.jaom.ws.trainings.UserPortType;

public class UserValidationImpl  implements UserPortType {
	
	User user = new User();
	
	
	public UserValidationImpl() {
		init();
	}
	public void init() {
		
		user.setName("admin");
		user.setPassword("1234");
		
	}
	@Override
	public GetUserResponse getUser(GetUserRequest request) {
		String name = request.getName();
		String password = request.getPassword();
		GetUserResponse response = new GetUserResponse();
		if(name.equals(user.getName()) && password.equals(user.getPassword())) {
			response.setResult(true);
		} 
		else response.setResult(false);
		return response;
		
	}
}
